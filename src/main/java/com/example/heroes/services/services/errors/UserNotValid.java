package com.example.heroes.services.services.errors;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "User is not valid")
public class UserNotValid extends RuntimeException {

    public UserNotValid(String message) {
        super(message);
    }
}
