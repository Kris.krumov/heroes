package com.example.heroes.services.services.implementations;

import com.example.heroes.data.models.User;
import com.example.heroes.data.repositories.UserRepository;
import com.example.heroes.services.models.auth.RegisterUserServiceModel;
import com.example.heroes.services.services.AuthService;
import com.example.heroes.services.services.HashingService;
import com.example.heroes.services.services.errors.UserNotValid;
import com.example.heroes.services.services.validations.AuthValidationService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {
    private final AuthValidationService authValidationService;
    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final HashingService hashingService;

    public AuthServiceImpl(AuthValidationService authValidationService, ModelMapper modelMapper, UserRepository userRepository, HashingService hashingService) {
        this.authValidationService = authValidationService;
        this.modelMapper = modelMapper;
        this.userRepository = userRepository;
        this.hashingService = hashingService;
    }

    @Override
    public void register(RegisterUserServiceModel model) {

        if (!authValidationService.isValid(model)){
            throw new UserNotValid("User is not valid");
        }
        User user = modelMapper.map(model,User.class);
        user.setPassword(hashingService.hash(user.getPassword()));
        userRepository.save(user);
    }
}
